Grobes Konzept (unfertiger Vorschlag) für B&B-Wissensdatenbank:


Wikidata ist das bisher erfolgreichste Community-basierte Projekt zur Wissensrepräsentation. In Wikidata werden Einträge der Wikipedia formal miteinander verknüpft.
Beispiel "Hannover" "ist_Hauptstadt_von" "Niedersachsen". Dadurch werden sehr flexible Abfragen möglich: Einfaches Beispiel: "Welche Hauptstädte deutscher Bundesländer haben mehr als 500000 Einwohner:innen?" Komplexeres Beispiel: An welchen Flüssen liegt mehr als eine Hauptstadt?

Das dahinterliegende [Datenmodell](https://www.mediawiki.org/wiki/Wikibase/DataModel#Overview_of_the_data_model) sieht folgendermaßen aus:
Es gibt `Items` ("Dinge auf die sich die Datenbank bezieht") und `Statements` (Aussagen über die Dinge). Jedes `Statement` besteht aus einem `Claim` ("Behauptung") und einer `Reference_list` ("Liste von Referenzen").

Wikidata orientiert sich also am Prinzip: "Wissen ist Wissen wos steht." Im Zweifelsfall geht es nicht um die Glaubwürdigkeit von Fakten sondern von Quellen.

Die Abfrage-Flexibilität erfordert ein hohes Maß an Formalisierung. Das Eintragen und Pflegen von Wissen in Wikidata wird durch spezialisierte Software unterstützt und erforder trotzdem Aufwand und Vorkenntnisse.


Nach dem was bisher abschätzbar ist, sind komplexe Abfragen wie oben nicht der Hauptanwendungszweck der B&B-Wissensdatenbank. Es geht darum, Informationen für die Community bereitzustellen, um folgende Aktivitäten zu ermöglichen:

- Sich selber zu konkreten Themen fortbilden
- Bildungs- und Aufklärungsarbeit leitsten
- Sich vernetzen


Beispiele für mögliche konkrete Fragen, die mit Hilfe der WDB beantwortet werden sollten:

- Wann ist persönliches Treffen nachhaltiger als Videokonferenz?
- Wie groß ist der CO2-Fußabdruck bestimmter digitaler Dienste?
- Wie lässt sich ältere Hardware länger nutzen?
- Welche Alternative gibt es zu XYZ?
- Welche Akteur:innen befassen sich mit dem Thema ABC?
- Welche IT-Unternehmen berücksichtigen Nachhaltigkeitsaspekte mit hoher Priorität?


Die Antwort muss dabei nicht direkt maschinenlesbar aus der Datenbank kommen, aber es sollte möglich sein, eine entsprechend verlinkte Quelle aufzusuchen und dort die Antwort möglichst schnell zu finden. Das heißt, die Einträge müssen über Schlagworte ("tags" bzw. "properties") gut findbar sein.

Von Wikidata kann die Hierarchisierung und die Flexibilität übernommen werden. Eventuell auch der Ansatz, konkrete Faktenbehauptungen als `Statement` zu repräsentieren.
Aus Kapazitäts- und Nutzerfreundlichkeitssgründen sollte unsere Lösung aber viel einfacher sein. Ziel: 10% der Möglichkeiten von Wikidata mit 0.01% des Aufwandes.

Umsetzungs-Idee: Wissen wird als YAML-Quelltext z.B. im Wiki uder in einem Repo hinterlegt und gepflegt. Es gibt eine handgestrickte Software (ggf. als Wiki-Plugin) mit welcher [konjuktive Anfragen](https://de.wikipedia.org/wiki/Konjunktive_Anfrage) an die Datenbank gestellt werden können.

Es wird zwischen "Meta-Wissen" (Datenmodell) und "Faktenwissen" (Nutzdaten) unterschieden. Die entsprechenden YAML-Dateien könnten so ausehen:

- [meta_data.yml](meta_data.yml)
- [data1.yml](data1.yml)

