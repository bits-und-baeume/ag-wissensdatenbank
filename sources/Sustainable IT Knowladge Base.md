# Literatur

 

* [https://cloud.google.com/resources/software-guide?ct=t(EMAIL\_CAMPAIGN\_2024-FEB-72)](https://cloud.google.com/resources/software-guide?ct=t\(EMAIL_CAMPAIGN_2024-FEB-72\))  
*  [https://unctad.org/publication/digital-economy-report-2024](https://unctad.org/publication/digital-economy-report-2024)  
* [https://github.com/re-cinq/green-tips](https://github.com/re-cinq/green-tips)  
* Buch: [Chancen einer Nachhaltigen IT](https://www.amazon.de/Chancen-einer-nachhaltigen-ressourceneffizienten-Softwareentwicklung/dp/3658401931/ref=sr_1_4?crid=2OQU8JW2QZRO1&dib=eyJ2IjoiMSJ9.q_XB5bIeWr9H5TiQ7jQBJeIK_39Zl6s3fAJ7FWxPhBXF-QgwpLxpfq8kFMWi6NU4YVIYfy6_laqu_N03vVRDnFkxdsz0_gV9bpJhHqYwknWc5JHo242vAxWNUb8mne57woPTtcnJRrRItHRHTh8ADKwj-YeWkmV1jAvF8NUlSvGPgsDkdSGlBZKziN_bXskQSO_z0V74qaq6XkvI_m1OIbo4drF_kbrBeh5Ct4Ba54g.oNpVDJWCDMc9km-OBNyk-UEiFueBItgJQ9Us88ZE0Ko&dib_tag=se&keywords=chancen+einer+nachhaltigen+it&nsdOptOutParam=true&qid=1736428703&sprefix=chancen+einer+na%2Caps%2C111&sr=8-4):   
* Buch: [Building Green Software](https://www.amazon.de/Building-Green-Software-Sustainable-Development/dp/1098150627/ref=sr_1_1?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=4TRZR1RMVHCA&dib=eyJ2IjoiMSJ9.4mQkjqRM3RvXZCFbuHPecvixACZt37OZ0zlB9Pa1VsqYkW5o2G_tWtzWkOR8fYh8jn1Wkw_ub6sfbkj7t6JMrA4B8-Strr61Els2_5Vw938qd19Qoqm8sXWlxcEvkfHQ8LZkLubQUl871bWJ70UmIgLjJQZ2aC5IBaLd0FSKQUZ1V6Z9sn7U5-itwl5d6ttWS5VZ1Hp8gPKvYuCbNtP2dG9bdpEALM8wJgwbUT0_2vM.Vz8RQi7oscW4iOCw6iIjfoeQy_8hXP-FfnuhFYQHF10&dib_tag=se&keywords=sustainable+it&nsdOptOutParam=true&qid=1736428580&s=books&sprefix=sustainable+it+%2Cstripbooks%2C113&sr=1-1) (O'Reilly):   
* iX Special \- Green IT: [https://shop.heise.de/ix-13-2022/Print](https://shop.heise.de/ix-13-2022/Print)

 

# Organizations & Websites

* [https://greensoftware.foundation/](https://greensoftware.foundation/)  
* [https://greenlab.di.uminho.pt/](https://greenlab.di.uminho.pt/)  
* [https://isit-europe.org/](https://isit-europe.org/)  
* [https://bits-und-baeume.org](https://bits-und-baeume.org/)  
  * Newsletter: [https://bits-und-baeume.org/newsletter/](https://bits-und-baeume.org/newsletter/)  
* [https://www.thegreenwebfoundation.org/](https://www.thegreenwebfoundation.org/)  
  * Newsletter: [https://www.thegreenwebfoundation.org/newsletter/](https://www.thegreenwebfoundation.org/newsletter/)  
* [https://os-climate.org/](https://os-climate.org/)  
* [https://www.futureengineers.org/](https://www.futureengineers.org/)  
* [https://www.wholegraindigital.com/curiously-green/](https://www.wholegraindigital.com/curiously-green/) (green web newsletter)

 

# Conferences

* [https://greenio.tech/](https://greenio.tech/)  
* [https://www.eco-compute.io/](https://www.eco-compute.io/)  
* [https://impact-festival.earth/](https://impact-festival.earth/)

 

# Standards und Normen

* [https://www.blauer-engel.de/de/produktwelt/software](https://www.blauer-engel.de/de/produktwelt/software)  
* ISO 14001 \- Umweltmanagementsystemnorm: [https://www.umweltbundesamt.de/themen/wirtschaft-konsum/wirtschaft-umwelt/umwelt-energiemanagement/iso-14001-umweltmanagementsystemnorm](https://www.umweltbundesamt.de/themen/wirtschaft-konsum/wirtschaft-umwelt/umwelt-energiemanagement/iso-14001-umweltmanagementsystemnorm)  
* ISO 50001 \- Energiemanagementnorm: [https://www.umweltbundesamt.de/energiemanagementsysteme-iso-50001](https://www.umweltbundesamt.de/energiemanagementsysteme-iso-50001)  
* ISO 26000 \- Social Responsability (nicht zertifizierbar): [https://www.iso.org/iso-26000-social-responsibility.html](https://www.iso.org/iso-26000-social-responsibility.html)  
* ISO

