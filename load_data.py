
"""
This script serves to validate the experimental yaml-files.
"""

import yaml
from ipydex import IPS, activate_ips_on_exception
activate_ips_on_exception()

fpath = "meta_data.yml"

with open(fpath, "r") as myfile:
    meta_data = yaml.safe_load(myfile)

fpath = "data1.yml"

with open(fpath, "r") as myfile:
    data1 = yaml.safe_load(myfile)

IPS()
